/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Type: Part of the framework
 * <b>Description</b>
 * - Inetaction utility for connecting with Cassandra Database Server
 * <b>Reference</b>
 * - https://gitlab.com/openfintechlab/medulla/project-docs/-/snippets/2011498
 */

 
import AppConfig                    from "../config/AppConfig";
import logger                       from "../utils/Logger";
import cassandra,  {Client, types}  from "cassandra-driver";



export default class CassandraInteractionUtil{
    private static cassandraClientObj: Client;
    

    /**
     * Initiate Pool connection with the database using configurations from the config store
     */
    public async initPoolConnectionWithConfig(){
        logger.debug(`Initializing Connection Pool with Config from Config Store`);
        logger.debug(`Configurations: DB User: ${AppConfig.config.OFL_MED_DB_USER} DB Connection Hosts: ${AppConfig.config.OFL_MED_DB_HOSTS}`);
        /**
         * OFL_MED_DB_USER      = Database User ID
         * OFL_MED_DB_PASS      = Database Password
         * OFL_MED_DB_HOSTS     = Comma seperate hosts / port properties e.g. <hostname>:<port>
         * OFL_MED_DB_DC        = Data center name
         * OFL_MED_DB_KEYSPACE  = Keyspace name
         * OLF_MED_DB_LCLDIS    = Local Distance
         * OFL_MED_DB_RMTDIS    = Remote Distance
         */
        // this.initPoolConnection(
        //     AppConfig.config.
        // )
        if(!(AppConfig.config.OFL_MED_DB_HOSTS || 
            AppConfig.config.OFL_MED_DB_DC ||
            AppConfig.config.OFL_MED_DB_KEYSPACE ||
            AppConfig.config.OFL_MED_DB_DC
            )){
            throw Error("Mandatory configuration not found required to connect to the database server");
        }else{
            await this.initPoolConnection(
                                AppConfig.config.OFL_MED_DB_HOSTS.split(','),   /* Configuration can be like {IP1}:{Port1},{IP2}:{Port2} */
                                AppConfig.config.OFL_MED_DB_DC,                 /* Datacenter name. e.g. 'datacenter1' */
                                AppConfig.config.OFL_MED_DB_KEYSPACE,           /* Keyspace name */
                                AppConfig.config.OLF_MED_DB_LCLDIS,             /* Local Distance */
                                AppConfig.config.OFL_MED_DB_RMTDIS,             /* Remote Distance */
                                AppConfig.config.OFL_MED_DB_USER,               /* DB User Name */
                                AppConfig.config.OFL_MED_DB_PASS                /* DB Password */
                                );        
        }        
    }
    
    /**
     * Initiate Pool connection with the database server
     * @param hosts Array of hosts:port pairs
     * @param dataCenter Datacenter name
     * @param keyspace Keysapce name
     * @param localDistance Local distance
     * @param remoteDistance Remote distance
     * @param dbUserName Database User Name
     * @param dbPassword Database Password
     */
    public async initPoolConnection(hosts: string[], dataCenter: string, keyspace: string, localDistance:number, remoteDistance:number, dbUserName?: string, dbPassword?: string ){

        CassandraInteractionUtil.cassandraClientObj = new Client({
            contactPoints: hosts,
            localDataCenter: dataCenter,
            keyspace: keyspace,
            credentials: {
                username: (dbUserName)? dbUserName:'' ,
                password: (dbPassword)? dbPassword: ''
            },
            pooling:{
                coreConnectionsPerHost:{
                    [cassandra.types.distance.local]: localDistance,
                    [cassandra.types.distance.remote]: remoteDistance
                },
            }
        });
        logger.info(`Connecting to Cassandra on hots: ${hosts}`);
        try{
            await CassandraInteractionUtil.cassandraClientObj.connect();
        }catch(error){
            logger.error(`Error received while connecting with database: ${error}`);
            throw error;
        }
    }

    /**
     * Close connection to the database server
     */
    public async closeConnection(){
        try{
            CassandraInteractionUtil.cassandraClientObj.shutdown();
        }catch(error){
            logger.error(`Error while shuting down the connection: ${error}`);
            throw error;
        }        
    }

    /** 
     * Get cassandra client static object
     */
    public get cassandraClient(){
        return CassandraInteractionUtil.cassandraClientObj;
    }

}