import AppConfig from "../config/AppConfig";

xdescribe('Configuration Loading test cases', () => {
    xtest('Get known configuration from the backend', async() => {
        try{            
            const response = await AppConfig.loadConfigurationfromConfigService('http://192.168.159.132:3000/configmanager/config/',"service-boiler-plate");
            expect(response).toBeDefined();
        }catch(error){
            fail(error);
        }                
    })

    xtest('Configuration does not exist', async() => {
        try{
            const response = await AppConfig.loadConfigurationfromConfigService('http://192.168.159.132:3000/configmanager/config/',"NOT THERE");            
            fail(response);            
        }catch(error){
            expect(error).toBeDefined();
        }   
    })

    /**
     * {"service-boiler-plate":[{"meta":{"version":"1.0","expired_on":"2020-07-04T17:23:55.857Z"},"NODE_ENV":"Production","OFL_MED_PORT":3000,"OFL_MED_CONTEXT_ROOT":"/boilerplate","OFL_MED_SRV_TIMEOUT":30,"OFL_MED_NODE_ENV":"info", "OLF_MED_TEST":  "TEST"}]}
     */
    xtest('Verify if environment variable have required VARS loaded', async() => {
        try{
            AppConfig.service_config_uri = 'http://localhost:32006/configmanager/config/';            
            const response = await AppConfig.loadConfigurationfromConfigService('http://localhost:32006/configmanager/config/',"service-boiler-plate");
            expect(response).toBeDefined();
            
            expect(process.env.OLF_MED_TEST).toBe('TEST');
        }catch(error){
            fail(error);
        }                
    })
});