import CassandraInteractionUtil from "../utils/CassandraInteractionUtil";


describe('Unit test-cases for testing Cassandra utility class', () => {
    test('Initiate Pool connection to the database server', async() => {
        var cassandraUtil:CassandraInteractionUtil = new CassandraInteractionUtil();
        try{
            await cassandraUtil.initPoolConnection(
                ['192.168.159.132:32020'],
                'datacenter1',
                'kadal0g01',
                2,
                1,          
                undefined,
                undefined      
            );
            console.log(cassandraUtil.cassandraClient.getState());
        }catch(error){
            console.log(`Error: ${error}`);
            expect(error).not.toBeDefined();
        }finally{
            console.log(`Closign the open connection`);
            await cassandraUtil.closeConnection();
        }
    });    

    test('Connect to the database withotut any configurations', async() =>{
        var cassandraUtil:CassandraInteractionUtil = new CassandraInteractionUtil();
        try{
            await cassandraUtil.initPoolConnectionWithConfig();
        }catch(error){
            console.log(`${error}`);
            expect(error).toBeDefined();
        }        
    });

    test('Connect to the database Using configuration from the config store', async() =>{
        var cassandraUtil:CassandraInteractionUtil = new CassandraInteractionUtil();
        try{
            await cassandraUtil.initPoolConnectionWithConfig();
        }catch(error){
            console.log(`${error}`);
            expect(error).toBeDefined();
        }        
    })
})